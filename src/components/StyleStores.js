import React, { Component } from 'react';

class StyleStores extends React.Component {
	constructor() {
		super()
	}

  	NumberFormat(numero, decimal, decimal_separador, milhar_separador) {
	    numero = (numero + '').replace(/[^0-9+\-Ee.]/g, '');
	    var n = !isFinite(+numero) ? 0 : +numero,
	            prec = !isFinite(+decimal) ? 0 : Math.abs(decimal),
	            sep = (typeof milhar_separador === 'undefined') ? ',' : milhar_separador,
	            dec = (typeof decimal_separador === 'undefined') ? '.' : decimal_separador,
	            s = '',
	            toFixedFix = function (n, prec) {
	                var k = Math.pow(10, prec);
	                return '' + Math.round(n * k) / k;
	            };
	    // Fix para IE: parseFloat(0.55).toFixed(0) = 0;
	    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	    if (s[0].length > 3) {
	        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	    }
	    if ((s[1] || '').length < prec) {
	        s[1] = s[1] || '';
	        s[1] += new Array(prec - s[1].length + 1).join('0');
	    }

	    return s.join(dec);
	}

  render() {

  	let valor = this.props.source;
  	var color = '';
  	//console.log(this.props.count);	
  	if(valor >= 15000.00){
		color = "text-defautl";
  	}else{
		color = "text-red";
  	}

    return (
    	<tr key={this.props.i}>
          <td className={color}>{this.props.nome}</td>
          <td className={color+" text-right"} >{this.NumberFormat(this.props.source, 2, ",", ".")}</td>
        </tr>
    );
  }
}

export default StyleStores;