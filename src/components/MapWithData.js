import React, { Component } from 'react';
import { Map, LeafletMap, TileLayer, Marker, Popup, Icon } from 'react-leaflet';

class MapWithData extends React.Component {
  constructor() {
    super()
  }

  ConstructMarker(){
    if(this.props.list.length > 0) {
      let storeListTable = '';
      storeListTable = this.props.list.map( function(obj, i) {
          if(i < 10){
            var newPosition = [obj.latitude, obj.longitude];
            return (
              <Marker position={newPosition} key={i}>
                <Popup>Unidade.<br />{obj.name}</Popup>
              </Marker>
            )  
          }
      })
      return storeListTable ;
    }
    
  }

  render() {
   var position = [-23.565972, -46.650859];
    return (
      <div className="custom-border">
        <Map center={position} zoom={13} >
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          />
          {this.ConstructMarker()}
        </Map>
      </div>
    );
  }
}

export default MapWithData;