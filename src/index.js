import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import App from './App';
import Page from './containers/Page';

ReactDOM.render(<BrowserRouter>
        <Switch>
            <Route path="/" exact={true} component={App} />
        </Switch>
    </ BrowserRouter>,
	document.getElementById('root'));
