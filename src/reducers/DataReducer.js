const initialState = {
    listOfAvailableClasses: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
    case 'CALENDAR_GET_INFORMATIONS_PENDING':
        state = { 
            ...state,
            gettingInformations: true
        }
        break;
    case 'CALENDAR_GET_INFORMATIONS_FULFILLED':
        state = {
            ...state,
            gettingInformations: false,
            listOfAvailableClasses: action.payload
        }
        break;
    case 'CALENDAR_GET_INFORMATIONS_REJECTED':
        state = {
            ...state,
            gettingInformations: false
        }
        break;
    default:
        break;
    }
    return state;
}

export default reducer