import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Button, Row, Col, Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import MapWithData  from '.././components/MapWithData';
import StyleStores from '.././components/StyleStores';
import axios from 'axios';

class Page extends Component {

  constructor() {
   super();
   this.state = {
      storesList: []
   };
  }
  
  componentWillMount() {
    axios.get('../data/data.json') // JSON File Path
    .then( response => {
      this.setState({
        storesList: response.data.stores
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  ClickSearch(){
    var elemento = document.getElementById("word");
    if(elemento.value == ""){
      alert("Informe um nome!");
    }else{
      alert("A funcao não foi completa input selecionado foi "+elemento.value);  
    }
    
  }

  MaperArrayStores(){
    
    if(this.state.storesList.length > 0) {
      let storeListTable = '';
      storeListTable = this.state.storesList.map( function(obj, i) {
          if(i <10){
            return (
              <StyleStores key={i} nome={obj.name} source={obj.revenue} count={i+1} />
            )  
          }
      })
      return storeListTable ;
    }

  }

  render() {
    
    return (
      <Row>
      	
        <Col md={12} className="bg-header">
      		<div className="nml-10 text-white">
      			<span className="stitle">Desempenho das Lojas</span>
      		</div>
      	</Col>

        <Col md={12}>
          <Col md={6}>

            <Col md={12}>
              <div className="input-group nmt-39">
                <input type="text" id="word" className="form-control" placeholder="Pesquisar"/>
                <span className="input-group-btn">
                  <button className="btn btn-default" type="button" onClick={()=>this.ClickSearch()}>
                    <div>
                      <FontAwesomeIcon icon={faSearch} />
                    </div>
                    </button>
                </span>
              </div>
            </Col>

            <Col md={12} className="nmt-10">
              <div className="custom-border">
                <Table>
                  <thead>
                    <tr>
                      <th>Loja</th>
                      <th>Faturamento</th>
                    </tr>
                  </thead>
                  <tbody>
                   { this.MaperArrayStores() }
                  </tbody>
                </Table>
                </div>
            </Col>
          </Col>

          <Col lg={6} md={6} sm={12} className="values">
            <Col lg={6} md={8} sm={8}>
              <h4>Faturamento mínimo esperado</h4>
              <div className="nmt-10 w100">
                <input type="text" className="form-control" placeholder="15.000,00"/>
              </div>
            </Col>
            <Col lg={12} md={12} sm={12} className="nmt-10">
              <MapWithData list={this.state.storesList} />
            </Col>
          </Col>
        </Col>
      </Row>
    );
  }
}

export default Page;
