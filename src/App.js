import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Button, Row, Col, Table, Pagination } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import MapWithData  from './components/MapWithData';
import StyleStores from './components/StyleStores';
import axios from 'axios';
import $ from 'jquery';

class App extends Component {

  constructor() {
   super();
   this.state = {
      storesList: [],
      storesListAll: []
   };
  }
  
  componentWillMount() {
    axios.get('../data/data.json') // JSON File Path
    .then( response => {
      this.setState({
        storesList: response.data.stores,
        storesListAll: response.data.stores
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  ClickSearch(){
    var elemento = $('#word');
    if(elemento.val() == ""){
      alert("Informe um nome!");
    }else{
      $('.pagination .item').removeClass("active");
      $(".item").eq(0).addClass('active');

      var newArrayPage = [];
      this.state.storesListAll.map( function(obj, i) {
        var nome = obj.name;
        if(nome.match(elemento.val())){
          newArrayPage.push(obj);
        }
      })
      this.setState({
        storesList: newArrayPage
      });
    }
    
  }

  MaperArrayStores(){
    
    if(this.state.storesList.length > 0) {
        let storeListTable = '';
        storeListTable = this.state.storesList.map( function(obj, i) {
            if(i <10){
              return (
                <StyleStores key={i} nome={obj.name} source={obj.revenue} count={i+1} />
              )  
            }
        })
        return storeListTable ;
      }
  }

  PaginateChoice(page){
    var newArrayPage = [];

    $('#word').val("");

    $('.pagination .item').removeClass("active");

    if(page == 1){
      $(".item").eq(0).addClass('active');
      this.state.storesListAll.map( function(obj, i) {
          if(i <10){
              newArrayPage.push(obj);
          }
      })
    }else if(page == 2){
      $(".item").eq(1).addClass('active');
      this.state.storesListAll.map( function(obj, i) {
          if(i >10 && i<= 20){
              newArrayPage.push(obj);
          }
      })

    }else if(page == 3){
      $(".item").eq(2).addClass('active');
      this.state.storesListAll.map( function(obj, i) {
          if(i >20 && i<=30){
              newArrayPage.push(obj);
          }
      })
    }else if(page == 4){
      $(".item").eq(3).addClass('active');
      this.state.storesListAll.map( function(obj, i) {
          if(i >30 && i<=40){
              newArrayPage.push(obj);
          }
      })
    }else if(page == 5){
      $(".item").eq(4).addClass('active');
      this.state.storesListAll.map( function(obj, i) {
          if(i >40){
              newArrayPage.push(obj);
          }
      })
    }

    this.setState({
        storesList: newArrayPage
    });


  }

  render() {
    
    return (
      <Row>
      	
        <Col md={12} className="bg-header">
      		<div className="nml-10 text-white">
      			<span className="stitle">Desempenho das Lojas</span>
      		</div>
      	</Col>

        <Col md={12}>
          <Col md={6}>

            <Col md={12}>
              <div className="input-group nmt-39">
                <input type="text" id="word" className="form-control" placeholder="Pesquisar"/>
                <span className="input-group-btn">
                  <button className="btn btn-default" type="button" onClick={()=>this.ClickSearch()}>
                    <div>
                      <FontAwesomeIcon icon={faSearch} />
                    </div>
                    </button>
                </span>
              </div>
            </Col>

            <Col md={12} className="nmt-10">
              <div className="custom-border">
                <Table>
                  <thead>
                    <tr>
                      <th>Loja</th>
                      <th className="text-right">Faturamento</th>
                    </tr>
                  </thead>
                  <tbody>
                   { this.MaperArrayStores() }
                  </tbody>
                </Table>
                </div>

                <div className="text-center">
                  <Pagination >
                    <Pagination.Item className="item active" onClick={() => this.PaginateChoice(1)}>{1}</Pagination.Item>
                    <Pagination.Item className="item" onClick={() => this.PaginateChoice(2)}>{2}</Pagination.Item>
                    <Pagination.Item className="item" onClick={() => this.PaginateChoice(3)}>{3}</Pagination.Item>
                    <Pagination.Item className="item" onClick={() => this.PaginateChoice(4)}>{4}</Pagination.Item>
                    <Pagination.Item className="item" onClick={() => this.PaginateChoice(5)}>{5}</Pagination.Item>
                  </Pagination>
                </div>

            </Col>
          </Col>

          <Col lg={6} md={6} sm={12} className="values">
            <Col lg={6} md={8} sm={8}>
              <h4>Faturamento mínimo esperado</h4>
              <div className="nmt-10 w100">
                <input type="text" className="form-control" placeholder="15.000,00"/>
              </div>
            </Col>
            <Col lg={12} md={6} sm={12} className="nmt-10">
              <MapWithData list={this.state.storesList} />
            </Col>
          </Col>
        </Col>
      </Row>
    );
  }
}

export default App;
